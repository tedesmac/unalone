import Vue from 'vue'

import Editor from './components/editor'


new Vue({
    el: '#editor',
    template: '<Editor />',
    components: { Editor }
})