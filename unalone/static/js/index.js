import Vue from 'vue'

import App from './components/index'


new Vue({
    el: '#app',
    template: '<App />',
    components: { App }
})